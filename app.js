const fetch = require('node-fetch');
const BME280 = require('bme280-sensor');

const apikey = '' //insert your thingspeak api key here

const updateUrl = '' //insert your thingspeak update url here

const getFieldUrl = 'https://api.thingspeak.com/channels/731067/fields/1.json?results='

async function httpGet(url, data1, data2, data3) {
    try {
        let response = await fetch(`${url}+&field1=${data1}+&field2=${data2}+&field3=${data3}`, {
            method: 'GET', // 'GET', 'PUT', 'DELETE', etc.
        });
        let json = await response.json();
        return json;
    }
    catch (error) {
        console.error(error);
    }
}

 
const options = {
    i2cBusNo   : 1, // defaults to 1
    i2cAddress : BME280.BME280_DEFAULT_I2C_ADDRESS() // defaults to 0x77
  };
  
const bme280 = new BME280(options);
  
  // Read BME280 sensor data, repeat
  //
const readSensorData = () => {
  bme280.readSensorData()
    .then((data) => {
      // temperature_C, pressure_hPa, and humidity are returned by default.
      // I'll also calculate some unit conversions for display purposes.
      
      httpGet(updateUrl, `${JSON.stringify(data.temperature_C, null, 2)}`, `${JSON.stringify(data.humidity, null, 2)}`, `${JSON.stringify(data.pressure_hPa, null, 2)}`);
      console.log(`${JSON.stringify(data, null, 2)}`);
      setTimeout(readSensorData, 20000);
    })
    .catch((err) => {
      console.log(`BME280 read error: ${err}`);
      setTimeout(readSensorData, 20000);
    });
};

// Initialize the BME280 sensor
//
bme280.init()
  .then(() => {
    console.log('BME280 initialization succeeded');
    readSensorData();
  })
  .catch((err) => console.error(`BME280 initialization failed: ${err} `));
